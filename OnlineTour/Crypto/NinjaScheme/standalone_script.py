import sys;
import os;
import random;

BLOCK_LEN = 8;   # Length of single Feistel block 
ROUNDS    = 256; # Amount of Feistel operation for each block
FLAG_PR   = "h4ck1t";

############################### F() Function ###############################

def F(L, RoundNum):
	LF = [];
	for i in range (0, len(L)):
		hex_code = int(L[i], 16);
		hex_code_modified = (hex_code + RoundNum) % 256;
		LF.append(hex(hex_code_modified));
	return LF;

############################### Cipher Routine ###############################

def SplitTextToBlocks(data):
	byte_blocks = [];

	for i in range(0, len(data)):
		buffer = [];
		piece = data[i*BLOCK_LEN : i*BLOCK_LEN+BLOCK_LEN];
		if len(piece) == 0: 
			break;
		if len(piece) < BLOCK_LEN:
			piece = piece.ljust(BLOCK_LEN, '0');
		for j in range(0, len(piece)):
			buffer.append(hex(ord(piece[j])));
		byte_blocks.append(buffer);

	return byte_blocks;


def SplitCipherToBlocks(data):
	data = ["0x"+data[i : i+2] for i in range(0, len(data), 2)];
	data = [data[i : i+BLOCK_LEN] for i in range(0, len(data), BLOCK_LEN)];
	return data;

def feistel(data, reverse):

	# Block splitting is differ during encrypting-decrypting
	bblocks = SplitCipherToBlocks(data) if reverse else SplitTextToBlocks(data);

	output = "";

	for N in range(0, len(bblocks)):
		for RoundNum in range(1, ROUNDS+1):

			L = bblocks[N][0:int(BLOCK_LEN/2)];
			R = bblocks[N][int(BLOCK_LEN/2):BLOCK_LEN];

			# Rounds are reversed during decrypting
			LF = F(L, (ROUNDS+1 - RoundNum) if reverse else RoundNum);

			LF_XOR_R = [];
			for i in range(0, int(BLOCK_LEN/2)): 
				xored_val = int(LF[i], 16) ^ int(R[i], 16);
				LF_XOR_R.append(hex(xored_val));

			bblocks[N] = [];
			bblocks[N].extend(LF_XOR_R);
			bblocks[N].extend(L);

		# Exchange Left and Right part of block for cipher 
		# to be prepared to decipher with Feistel Network
		L = bblocks[N][0:int(BLOCK_LEN/2)];
		R = bblocks[N][int(BLOCK_LEN/2):BLOCK_LEN];
		bblocks[N] = R; bblocks[N].extend(L);

		output += "".join([byte[2:4].rjust(2,'0') for byte in bblocks[N]]);

	return output;

############################### Entry Routine ###############################

def GenTask():

	flags = [
				"00_F315T31_Kn0VVS_H1S_NN3Tvv0Rk_PrEttY_a1nt_B4D"
			];

	flag = flags[random.randint(0, len(flags)-1)];
	flag = list(flag);

	for i in range(0,2):
		flag[i] = chr(random.randint(65,122));
		if 90 < ord(flag[i]) and ord(flag[i]) < 97:
			flag[i] = chr(ord(flag[i])-10);

	flag = FLAG_PR + "{" + "".join(flag) + "}";
	cipher_flag = feistel(flag, False);

	# Task text
	print("\nOur scouts have intercepted this enemy cryptogram: \n"+cipher_flag);
	print("Some time later our IT-ninjas have broken into the enemy computer system");
	print("and grabbed something pretty much similar to encryption algorithm scheme.");
	print("Look at this grabbed scheme and help us to understand how it works.");

	print("");
	print("Original flag = "+flag+"\n");

	# Return for UnitTest`ing #
	return [flag, cipher_flag];


def Solve(cryptogramm):

	# Participant doesn`t exactly know the amount of rounds.
	for i in range(1, 1024):
		ROUNDS = i;
		decipher = feistel(cryptogramm, True);

		source = "";
		for i in [decipher[i : i+2] for i in range(0, len(decipher), 2)]:
			source += chr(int(i,16));

		if source.find(FLAG_PR)>-1:
			print(source);
			break;
	
	# Return for UnitTest`ing #
	return source;

############################### Entry Point ###############################

def UnitTest():

	for i in range (0, 20000):
		flag, cipher_flag = GenTask();
		hack = Solve(cipher_flag);

		if(hack.find(flag)>-1):
			print(i);
			print("===GEN: "+flag);
			print("===GOT: "+hack+"\n");
		else:
			print(i);
			break;

		os.system('cls' if os.name == 'nt' else 'clear')


def main():
	argc = len(sys.argv);

	if argc == 1 or sys.argv[1].lower() == "gen":
		GenTask();

	if argc == 3 and sys.argv[1].lower() == "solve":
		Solve(sys.argv[2]);

	if argc == 2 and sys.argv[1].lower() == "test":
		UnitTest();


if __name__ == "__main__":
	main();
	