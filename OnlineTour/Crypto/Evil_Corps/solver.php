<?php

$freqs = array(
		'A' => 8.17,
		'B' => 1.49,
		'C' => 2.78,
		'D' => 4.25,
		'E' => 12.7,
		'F' => 2.23,
		'G' => 2.02,
		'H' => 6.09,
		'I' => 6.97,
		'J' => 0.15,
		'K' => 0.77,
		'L' => 4.03,
		'M' => 2.41,
		'N' => 6.75,
		'O' => 7.15,
		'P' => 1.93,
		'Q' => 0.10,
		'R' => 5.99,
		'S' => 6.33,
		'T' => 9.06,
		'U' => 2.76,
		'V' => 0.98,
		'W' => 2.36,
		'X' => 0.15,
		'Y' => 1.97,
		'Z' => 0.05
	);


function uni_strsplit($string, $split_length=1)
{
    preg_match_all('`.`u', $string, $arr);
    $arr = array_chunk($arr[0], $split_length);
    $arr = array_map('implode', $arr);
    return $arr;
}

$alphabet = array();
$data = file_get_contents("task.txt");
$data = uni_strsplit($data);

$total = 0;
foreach($data as $s) {

	if(ctype_space($s))
		continue;

	if(!isset($alphabet[$s]))
		$alphabet[$s] = 1;
	$alphabet[$s]++;
	$total++;
}

foreach ($alphabet as $key => $val) {
	$alphabet[$key] = number_format(($val/$total * 100.0), 2);
	//echo $key." ---> ".number_format(($val/$total * 100.0), 2)."\n";
}

print_r($alphabet);

arsort($alphabet);
arsort($freqs);

/*$possibles = array();
foreach($alphabet as $lett=>$freq)
{
	$possibles[$lett] = array();

	foreach($freqs as $key=>$val)
	{
		if(abs($val-$freq)<1.0)
			array_push($possibles[$lett], $key);
	}
}

echo "==================================";*/
//print_r($possibles);

$alphabet_keys = array_keys($alphabet);
$freqs_keys = array_keys($freqs);

for($i=0; $i<count($alphabet_keys); $i++) {
	$alphabet[$alphabet_keys[$i]] = $freqs_keys[$i];
}

//print_r($alphabet);

$result = str_replace(
	array_keys($alphabet), 
	array_values($alphabet), 
	file_get_contents("task.txt")
	);

$result = str_replace(
	array('S','Y','M','F','G'),
	array('H','G','W','Y','F'),
	$result
	);

echo $result;

?>