<?php

$alphabet = array(
		'A' => 'ӕ',
		'B' => 'β',
		'C' => 'Ͻ',
		'D' => 'Δ',
		'E' => 'Σ',
		'F' => '∫',
		'G' => '9',
		'H' => 'Ϧ',
		'I' => '¦',
		'J' => 'τ',
		'K' => 'ϟ',
		'L' => 'λ',
		'M' => 'ᛗ',
		'N' => 'ɲ',
		'O' => 'θ',
		'P' => '∏',
		'Q' => 'ⵕ',
		'R' => 'ʁ',
		'S' => '§',
		'T' => '+',
		'U' => 'µ',
		'V' => '✔',
		'W' => 'ω',
		'X' => '✕',
		'Y' => '¥',
		'Z' => 'ɀ'
	);

$flags = array(
	"MASSONSAREEVERYWHERELOOKBEHINDYOU"
	);

function TextToLang($text)
{
	global $alphabet;
	$out = "";
	
	foreach(str_split(mb_strtoupper($text)) as $ch){
		//if($ch == ' ')  {continue;}
		if(in_array($ch, array("\n", " "))) {
			$out .= $ch; 
			continue;
		}
		if(!isset($alphabet[$ch])) continue;
		$out .= $alphabet[$ch];
	}

	return $out;
}

$flag = $flags[rand()%count($flags)];
$payload = file_get_contents("payload.txt");
$payload = str_replace(chr(13), "", $payload);
$payload = str_replace("{flag}", $flag, $payload);

echo "Our forensic team have already been investigating\n";
echo "\"The Great Plantation\" project for 2 years.\n";
echo "During this long period we knew a lot of interesting things like:\n";
echo "1) This is the project of Masons.\n";
echo "2) The main target is to take over the world with planting own people into big companies.\n";
echo "3) Masons do not trust modern encryption algorithms.\n";
echo "4) Masons think their own alphabet will let them to communicate more securely than anything else.";
echo "\n";
echo "Short time ago we have withdrawn the notebook of one of their members.\n";
echo "There was one interesting file named \"THPlantation.txt\".\n";
echo "Surely, it was encrypted. Help us to pull the info out of this!\n";

echo "\n\nFlag: h4ck1t{".$flag."} \n\n";

file_put_contents("task.txt", TextToLang($payload))

?>