<?php

// Custom extension from ../ext_ctf
dl('php_bcompiler.dll');

// Compilation
/*$fh = fopen("PhParanoid.phb", "w");
bcompiler_write_header($fh);
bcompiler_write_file($fh, "task.php"); 
bcompiler_write_footer($fh);
fclose($fh);*/

// Code which leads straight to flag
$secret = "ThereIsNoRightAndWrong.ThereOnlyFunAndBoring.";

// Include zend opcodes
require 'CTF/PhParanoid.phb';

// Investigation of above execution
//print_r(get_defined_vars());

$res = "";
foreach (get_defined_vars() as $key => $value) {
	$res .= $value;
}

echo $res;

// Generation of task for task.php
/*$flag = "h4ck1t{O0h_0pC0D35_G0t_1N51D3_MiN3_51CK_M1nD}";
$hash = "ThereIsNoRightAndWrong.ThereOnlyFunAndBoring.";
for($i = 0; $i<strlen($flag); $i++)
{
	echo '$c'.$i.' = chr(ord($secret['.$i.']) +'.(ord($flag[$i])-ord($hash[$i])).');';
	echo 'if(ord($c'.$i.') +'.(ord($hash[$i])-ord($flag[$i])).' != '.ord($hash[$i]).'){unset($c'.$i.'); break;}';
	echo "\n";
}
*/

?>