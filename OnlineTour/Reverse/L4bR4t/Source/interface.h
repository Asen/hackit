
#ifndef INTERFACE_H
#define INTERFACE_H

/*************************** HEADER FILES ***************************/
#include <stddef.h>

/****************************** MACROS ******************************/
#define BLOCK_SIZE 16               // AES operates on 16 bytes at a time

/**************************** DATA TYPES ****************************/
typedef unsigned char BYTE;            // 8-bit byte
typedef unsigned int WORD;             // 32-bit word, change to "long" for 16-bit machines


class SuperSecretLabCryptor2000 { 

	public:
		
		SuperSecretLabCryptor2000 ();
		void CryptFile(char* filename);

  	private:

  		// custom randomization seed
  		unsigned int TIME;
  		// default key
		BYTE key[32] = {0}; 
		// default vector
		BYTE iv[16] = {0};


		void init_key();
		void init_iv();	


	  	/*********************** FUNCTION DECLARATIONS **********************/
		///////////////////
		// AES
		///////////////////
		// Key setup must be done before any AES en/de-cryption functions can be used.
		void key_setup(const BYTE key[],          // The key, must be 128, 192, or 256 bits
		               WORD w[],                  // Output key schedule to be used later
		               int keysize);              // Bit length of the key, 128, 192, or 256

		void encrypt(const BYTE in[],             // 16 bytes of plaintext
		             BYTE out[],                  // 16 bytes of ciphertext
		             const WORD key[],            // From the key setup
		             int keysize);                // Bit length of the key, 128, 192, or 256

		void decrypt(const BYTE in[],             // 16 bytes of ciphertext
		             BYTE out[],                  // 16 bytes of plaintext
		             const WORD key[],            // From the key setup
		             int keysize);                // Bit length of the key, 128, 192, or 256

		///////////////////
		// AES - CBC
		///////////////////
		int encrypt_cbc(const BYTE in[],          // Plaintext
		                size_t in_len,            // Must be a multiple of AES_BLOCK_SIZE
		                BYTE out[],               // Ciphertext, same length as plaintext
		                const WORD key[],         // From the key setup
		                int keysize,              // Bit length of the key, 128, 192, or 256
		                const BYTE iv[]);         // IV, must be AES_BLOCK_SIZE bytes long
		// Only output the CBC-MAC of the input.

		int decrypt_cbc(const BYTE in[],          // plaintext
		                size_t in_len,            // Must be a multiple of AES_BLOCK_SIZE
		                BYTE out[],               // Output MAC
		                const WORD key[],         // From the key setup
		                int keysize,              // Bit length of the key, 128, 192, or 256
		                const BYTE iv[]);         // IV, must be AES_BLOCK_SIZE bytes long
};

#endif