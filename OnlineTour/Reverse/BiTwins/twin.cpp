#include <iostream>
#include <cstdio>
#include <string.h>
#include <cstdlib>
#include <time.h>
#include <unistd.h> 
using namespace std;

#define PAUSE (sleep(1));

/////////////////////////////////////////////////////////////

void MaskFlag(char* flag);
void TwinIsGonnaRunHisLifeCycle(unsigned char a);
void TwinIsGonnaRunHisLifeCycle(char a);
char GetDataByKey(int idx);

/////////////////////////////////////////////////////////////

char FNAME[] = "BiTwins.bi";

// Twin 1

char data[] = {'N', '=', 't', 'g', 'w', '3', 'b', '=', '-', 'k', 'r', 'e', '0', '2', 'n', 'n', 'e', 'e', '=', '-', 'k', 'h', '-', '0', '=', 'v', 'M', 'e', 'o', 't', 'h', 'C', 'R', 'W', '_', 'a', 'b', 'o', 'c', '4', '=', 'o', '=', 'T', 't', 'r', 'n', 'r', 'R', '-', 'P', '0', 'e', '_', 'r', 'H', '}', 's', 's', 'T', '3', 's', '3', 'a', 'i', '5', '=', '_', '=', '1', 'q', 's', 'r', 'z', 't', 'n', 'a', 'z', 'n'};
int  keys[] = {0, 24, 1, 3, 30, 50, 6, 34, 17, 45, 10, 15, 78, 54, 5, 21, 8, 23, 43, 7, 52, 27, 13, 71, 29, 48, 55, 14, 18, 11, 20, 56, 47, 60, 62, 12, 67, 38, 4, 44, 32, 40, 37, 51, 46, 70, 31, 64, 49, 22, 36, 77, 19, 63, 76, 42, 66, 16, 28, 9, 59, 39, 61, 73, 2, 53, 26, 57, 41, 58, 72, 74, 33, 75, 68, 25, 35, 69, 65};
char born[] = "I am the #1st born twin!";

// Twin 2
/*
char data[] =  {'e', 'o', 'h', 't', 'u', 'a', 'N', '-', '_', 'h', 'n', 'e', 'r', 'i', 'v', 'o', '_', 'e', 'r', '_', '4', 'K', 'e', '0', '3', 'u', 'g', 'p', 'o', 'c', 'h', '5', 'U', 'd', 'b', 'h', 't', 't', 'n', 'i', '3', '3', 'e', '_', '_', 'A', 'E', 's', 'v', 'e', 'e', 'm', 'h', 'e', '{', 'h', 'i', 'e', '0', '0', '=', '-', 'T', '-', 'l', 'y', '_', 'a', 'm', 'w', 'n', 's', 't', 'U', 'w', 'h', 't', 'D', 'T'};
int  keys[] = {71, 76, 60, 19, 67, 4, 61, 10, 77, 39, 2, 28, 15, 30, 14, 0, 78, 9, 13, 50, 51, 65, 69, 74, 52, 72, 21, 35, 70, 44, 1, 49, 64, 25, 7, 43, 29, 26, 12, 45, 47, 48, 16, 54, 59, 24, 42, 17, 55, 6, 27, 58, 11, 33, 46, 34, 20, 75, 66, 68, 38, 5, 41, 3, 73, 36, 53, 32, 18, 40, 22, 31, 37, 62, 23, 56, 8, 63, 57};
char born[] = "I am the #2nd born twin!";
*/
/////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
	if(argc >= 3)
	{
		/*if( !strcmp(argv[1], "maskflag") )
		{
			MaskFlag(argv[2]);
			return 0;
		}*/

		if( !strcmp(argv[1], "twin") && strlen(argv[2])>3)
		{
			bool flag = true;
			for(int i=9; i<9+4; i++)
				if(born[i] != argv[2][i-9]) 
					TwinIsGonnaRunHisLifeCycle((unsigned char)'A');

			TwinIsGonnaRunHisLifeCycle((char)'B');
		}
	}
}


void TwinIsGonnaRunHisLifeCycle(char a)
{
	int  dataLen = sizeof(data) / sizeof(char);
	int  idx = 0;
	char lastChar = '\0';


	while(idx < dataLen-1)
	{
		FILE* f = fopen(FNAME, "r");

		// File doesn`t exist
		if(!f)
		{
			f = fopen(FNAME, "w");
			if(!f) continue;

			char nChar = GetDataByKey(idx);
			if( fputc(nChar, f) == nChar ) 
			{
				idx ++;
				lastChar = nChar;
				//cout << lastChar << endl;
				cout << "Hey, are u there, my brother-twin?" << endl;
			}
			fclose(f);			
			PAUSE;
			continue;
		}

		char input;
		fscanf(f, "%c", &input);
		fclose(f);
		PAUSE;

		/* File exists and it was touched by something else */
		if(input != lastChar)
		{	
			f = fopen(FNAME, "w");
			if(!f) continue;

			char nChar = GetDataByKey(idx);
			if( fputc(nChar, f) == nChar ) 
			{
				idx ++;
				lastChar = nChar;
				//cout << input << endl << lastChar << endl;
				cout << "I hear you, brother-twin!" << endl;
			}
			fclose(f);
			PAUSE;
			continue;
		}

		//cout << input << " --- " << lastChar << " --- " << idx << endl;

	}

	cout << "Bye, my brother-twin!" << endl;
	//remove(FNAME);
}


void TwinIsGonnaRunHisLifeCycle(unsigned char a)
{
	const int NotDebuggableBelow = -1;
	exit(0);
}


char GetDataByKey(int idx)
{
	int keysLen = sizeof(keys) / sizeof(int);
	for(int i=0; i<keysLen; i++)
	{
		if(keys[i] == idx) return data[i];
	}

	return '\0';
}

/*

void MaskFlag(char *flag)
{
	// Initial Generation 
	int flen = strlen(flag);
	int twin_buff_sz = flen/2;

	char* twin1         = (char*)calloc( twin_buff_sz, sizeof(char) );
	int*  twin1_keys = (int*)calloc( twin_buff_sz, sizeof(int) );

	char* twin2         = (char*)calloc( twin_buff_sz, sizeof(char) );
	int*  twin2_keys = (int*)calloc( twin_buff_sz, sizeof(int) );

	int index = 0;

	for(int i=0; i<flen; i++)
	{
		if(i%2==0) {
			twin1[index]         = flag[i];
			twin1_keys[index] = index;
		}
		else
		{
			twin2[index]         = flag[i];
			twin2_keys[index] = index;

			index++;
		}
	}

	// Mixing 
	srand(time(0));
	for(int i=0; i<1024; i++)
	{
		int ind1 = rand() % twin_buff_sz;
		int ind2 = rand() % twin_buff_sz;

		if(ind1 == ind2) continue;

		// Mix twin1`s data 
		char tmp_c = twin1[ind1];
		twin1[ind1] = twin1[ind2];
		twin1[ind2] = tmp_c;
		int tmp_i = twin1_keys[ind1];
		twin1_keys[ind1] = twin1_keys[ind2];
		twin1_keys[ind2] = tmp_i;

		ind1 = rand() % twin_buff_sz;
		ind2 = rand() % twin_buff_sz;

		// Mix twin2`s data 
		tmp_c = twin2[ind1];
		twin2[ind1] = twin2[ind2];
		twin2[ind2] = tmp_c;
		tmp_i = twin2_keys[ind1];
		twin2_keys[ind1] = twin2_keys[ind2];
		twin2_keys[ind2] = tmp_i;
	}

	//  Result output 
	//cout << "Twin 1:" << endl;
	
	cout << "char data[] = {";
	for(int i=0; i<twin_buff_sz; i++){
		cout << "'" << twin1[i] << "'";
		if(i != twin_buff_sz-1) cout << ", "; 
	}
	cout << "};" << endl;
	cout << "int keys[] = {";
	for(int i=0; i<twin_buff_sz; i++){
		cout << twin1_keys[i];
		if(i != twin_buff_sz-1) cout << ", "; 
	}
	cout << "};" << endl;



	cout<<endl<<endl;



	//cout << "Twin 2:" << endl;
	
	cout << "char data[] = {";
	for(int i=0; i<twin_buff_sz; i++){
		cout << "'" << twin2[i] << "'";
		if(i != twin_buff_sz-1) cout << ", "; 
	}
	cout << "};" << endl;
	cout << "int keys[] = {";
	for(int i=0; i<twin_buff_sz; i++){
		cout << twin2_keys[i];
		if(i != twin_buff_sz-1) cout << ", "; 
	}
	cout << "};" << endl;
	//////////////////////////////////////

}

*/