#include <iostream>
#include <cstring>
#include <cstdlib>


// h4ck1t{36f35433c667031c203b42d5a00fe194}

char correct[41] = "o3dl6s|41a42344d110746d574e35c2f77ab6>3z";
using namespace std;
class sub_4400
{
	private:
		char flag[40];
	public:
		sub_4400(char *a)
		{
			memcpy(flag,a,sizeof(flag));
		}
		sub_4400(){
		}
		inline void get_flag()
		{
			for (int i = 0 ; i < 40 ; i++) cout << flag[i];
		}
		inline void checker()
		{
			if(!memcmp(flag,correct,sizeof(flag))) cout << "Access allowed!" << endl;
			else cout << "Access denied!" << endl;
		}
		friend sub_4400 operator+(sub_4400 ar1,int ar2);
		friend void hash_obj(sub_4400 &obj, int c);		
		
};

sub_4400 operator+(sub_4400 ar1, int ar2)
{
	sub_4400 temp;
	for (int i = 0 ; i < 40 ; i++) temp.flag[i] = ar1.flag[i] ^ ar2;
	return temp; 
}

void hash_obj(sub_4400 &obj, int c)
{
	sub_4400 buf;
	buf = obj + c ;
	obj = buf;
}

int main()
{
	char b_f[40];
	cout << "Th3 k3y: " << endl;
	gets(b_f);
	sub_4400 fl(b_f);
	
	for (int i = 0 ; i < 7 ; i++) hash_obj(fl,i);
	
	cout << endl;
	fl.checker();
		
	return 0;
}
