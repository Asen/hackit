# README #

This repo contains all the tasks from HackIT 2016 CTF (online and offline). 
Crypto and Reverse categories.

### What is this repository for? ###

I've been responsible for Reverse and Crypto categories on HackIt2016. 
So now you have the opportunity get to know the tasks` logic and their solutions.

### Who do I talk to? ###

* AseN - the creator
* [Invulnerable CTF Team](http://countersite.org/) - organizer