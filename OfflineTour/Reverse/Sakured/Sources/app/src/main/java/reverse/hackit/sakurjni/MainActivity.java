package reverse.hackit.sakurjni;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    // Used to load the native library on application startup.
    static {
        System.loadLibrary("sakurmodule");
    }
    /**
     * A native method that is implemented by the native library,
     * which is packaged with this application.
     */
    public native boolean chp0ck3r(String s);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText chpockerEdit = (EditText)findViewById(R.id.chpocker);
        findViewById(R.id.mover).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_input = chpockerEdit.getText().toString();
                Log.i("JNIII", chp0ck3r(user_input)+"");
            }
        });

    }
}
