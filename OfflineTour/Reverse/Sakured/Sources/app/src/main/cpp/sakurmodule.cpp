#include <jni.h>
#include <string>

////////////////////////////////////////////////////////////////////////////////////////////////////

int sakurdata[40] = {
        155, 178, 155, 135, 152, 152, 178, 212, 178, 133, 183, 192, 144, 178, 155, 130,
        192, 133, 181, 152, 202, 152, 133, 133, 209, 155, 186, 183, 140, 153, 183, 197,
        152, 153, 178, 192, 181, 203, 183, 183
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class subst
{
    protected:
        int substdat[40];

    public:

        subst (int *a)
        {
            for (int i = 0 ; i < 40 ; i++)
                substdat[i] = a[i];
        }

        subst operator--();
    };

subst subst::operator--()
{
    for (int i = 0 ; i < 40 ; i++)
        substdat[i] -= 1;
    return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

class sakur1 : public subst
{
    private:
        char sakur1dat[40];

    public:

        sakur1(char *a,int *b) : subst(b)
        {
            for (int i = 0 ; i < 40 ; i++ ) sakur1dat[i] = a[i];
        }

        friend sakur1 operator+(sakur1 ar1, int ar2);
        sakur1 operator++();

        char* extract_sakurdat()
        {
            return sakur1dat;
        }
        bool correct_flag()
        {
            return memcmp(sakur1dat, sakurdata, sizeof(sakur1dat)) == 0;
        }
};

sakur1 sakur1::operator++()
{
    char tmp_buff[40];
    memcpy(tmp_buff,sakur1dat,sizeof(sakur1dat));
    for(int i = 0 ; i < 40 ; i++)
        sakur1dat[i] = tmp_buff[substdat[i]];

    return *this;
}

sakur1 operator+(sakur1 ar1, int ar2)
{
    sakur1 tmp_obj(ar1.sakur1dat, ar1.substdat);
    for (int i = 0 ; i < 40 ; i++)
        tmp_obj.sakur1dat[i] = ar1.sakur1dat[i] ^ ar2;
    return tmp_obj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

class sakur2
{
    private:
        int sakur2dat[40];

    public:
        sakur2(int *a)
        {
            for (int i = 0 ; i < 40 ; i++)
                sakur2dat[i] = a[i];
        }
        void sakured()
        {
            for (int i = 0 ; i < 40 ; i++)
                sakur2dat[i] += 2;
        }
        void sakured(int n)
        {
            for (int i = 0 ; i < 40 ; i++)
                sakur2dat[i] += n;
        }
        void sakured(int n, int k)
        {
            for (int j = 0 ; j < k ; j++)
                for (int i = 0 ; i < 40 ; i++)
                    sakur2dat[i] += n;
        }
        void sakured(int n, int k, int l)
        {
            for (int j = 0 ; j < k ; j++)
                for (int i = 0 ; i < 40 ; i++)
                {
                    sakur2dat[i] += n;
                    sakur2dat[i] ^= l;
                }
        }
        void sakured(int n, int k, int l, int m)
        {
            int sum = n + k + l + m;
            for (int i = 0 ; i < 40 ; i++)
                sakur2dat[i] += sum;
        }
        bool chp0k3r()
        {
            return memcmp(sakur2dat,sakurdata,sizeof(sakur2dat))==0;
        }
};

extern "C" jboolean Java_reverse_hackit_sakurjni_MainActivity_chp0ck3r(
        JNIEnv *env, jobject obj, jstring strobj)
{

    char * cStringFromNative;
    const char * cStringFromNative_ = env->GetStringUTFChars(strobj, 0);
    cStringFromNative = strdup (cStringFromNative_);
    env->ReleaseStringUTFChars(strobj, cStringFromNative_);
    char s1[40];
    memcpy(s1,cStringFromNative,40);
    free (cStringFromNative);

    int r1[40] = {
            37,21,14,22,1,20,15,27,12,32,6,31,17,30,13,24,11,4,9,28,
            10,7,16,33,34,38,35,39,25,26,2,36,19,23,0,3,5,29,18,8
    };
    int r2[40] = {
            29,13,8,2,14,33,10,21,24,22,36,26,38,25,17,0,4,6,27,5,35,
            39,9,1,16,31,20,37,34,11,23,28,15,12,18,3,19,30,32,7
    };
    int r3[40] = {
            22,2,26,33,17,9,13,29,7,39,18,5,38,3,1,34,19,6,0,24,28,27,
            31,21,32,30,20,35,23,8,15,10,12,14,37,11,4,16,36,25
    };
    sakur1 h1(s1,r1);
    ++h1;
    sakur1 h2(s1,r2);
    h2 = h1 + 18;
    sakur1 h3(s1,r3);
    h3 = h2 + 23;
    ++h3;

    int s2[40];
    for (int i = 0 ; i < 40 ; i++)
        s2[i] = int(h3.extract_sakurdat()[i]);
    sakur2 store(s2);
    sakur2 *p;
    p = &store;
    p->sakured();
    p->sakured(3);
    p->sakured(4, 3);
    p->sakured(5, 4, 6);
    p->sakured(1, 2, 3, 4);
    p->sakured(6, 4, 5);
    p->sakured(3, 4);
    p->sakured(7);
    p->sakured();

    return p->chp0k3r() ? JNI_TRUE : JNI_FALSE;
}
