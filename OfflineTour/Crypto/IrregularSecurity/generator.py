import datetime
import random
import sys
import os

def encrypt(m, e, n):
    return pow(m, e, n)

def check(p):
    return os.popen("openssl prime "+str(p)).read().find("not")==-1 and p%4 == 0x3

salt = random.randint(1,11)
seed = datetime.datetime.now().second * salt
seed = 287
random.seed(seed);

p=random.randint(pow(10,59), pow(10,60))
while not check(p):
	p += 1;
q=random.randint(pow(10,69), pow(10,70))
while not check(q):
	q += 1;

n = p*q;
m = int("".join([str(hex(ord(ch)))[2:] for ch in sys.argv[1]]), 16)
c = encrypt(m, 2, n)

# Just do my best to protect myself:
for i in range(0,512):
    c = encrypt(c, 2, n)

print seed
print c