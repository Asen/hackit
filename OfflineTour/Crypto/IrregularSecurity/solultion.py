import datetime
import random
import os


def encrypt(m, e, n):
    return pow(m, e, n)


# returns g, x, y such that ax + by = 1
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def decrypt(p, q, c):
    # Use Extended Euclid's Algorithm to find x and y such that px+qy=1
    (g, x, y)=egcd(p,q)
 
    # Calculate n
    n=p*q;
 
    # Calculate square roots in Zp and Zq
    r=(pow(c,((p+1)//4),p))
    s=(pow(c,((q+1)//4),q))
 
    # Use the Chinese Remainder Theorem to find 4 square roots in Zn
    r1=((x*p*s)+(y*q*r))%n
    r2=((x*p*s)-(y*q*r))%n
    r3=(-r1)%n
    r4=(-r2)%n
 
    return r1, r2, r3, r4

CC = 1444400819803309393395024335756316466876954623536978573354842285237599086498975438996492773968612039402519681344595905950447966496
flag = "".join([str(hex(ord(ch)))[2:] for ch in "h4ck1t"])


print datetime.datetime.now()


for salt in range(7,12):
    for min in range(30,60):

        seed = min*salt
        print " >>> >>> "+str(seed)
        random.seed(seed)

        p=random.randint(pow(10,59), pow(10,60));
        while (os.popen("openssl prime "+str(p)).read()).find("not")>1 or p%4!=3:
            p += 1;
        q=random.randint(pow(10,69), pow(10,70))
        while (os.popen("openssl prime "+str(q)).read()).find("not")>1 or q%4!=3:
            q += 1;
            
        n=p*q;
        c = CC;

        for i in range(0,512):
            c = decrypt(p,q,c)[3]

        for dec in decrypt(p,q,c):
            s = str(hex(dec))
            if s.find(flag)>0:
                print s
                print datetime.datetime.now()
                quit();