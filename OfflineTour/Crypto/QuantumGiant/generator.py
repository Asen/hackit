import random



### Alice ###

flag   = "h4ck1t{1t_l00k5_l1k3_qu4ntum_r3p14535_m4th!!!}"
gamma  = "_h31l0_b0bby_us3_th15_g4mm4_w1th_byt3_m0du1u5_"
cipher = []

for i in range(0, len(flag)):
	cipher.append((ord(flag[i])+ord(gamma[i])) % 256)

#print "Quatum: " + quantum_sequense
print "[A ---> B] (public channel)\n"
print "".join([str(hex(cipher_char))[2:] for cipher_char in cipher])
print "\n===============================================================";

key_binary = ""; # key A and B gonna find from quantum chanell
for gamma_char in gamma:
	key_binary += str(bin(ord(gamma_char)))[2:].rjust(8,'0')

alice_quantum_sequense = ""
for i in range(0, 4096):
	rand = random.randint(1,4);
	if rand==1: alice_quantum_sequense += "-";
	if rand==2: alice_quantum_sequense += "/";
	if rand==3: alice_quantum_sequense += "|";
	if rand==4: alice_quantum_sequense += "\\";

#print "Quantum key: "  + key_binary
print "[A ---> B] (quantum channel)\n"
print alice_quantum_sequense
print "\n===============================================================";



### Bob ###

bob_measure_types = ""
key_binary = [ch for ch in key_binary]
key_idx = 0
i = 0;

while i < len(alice_quantum_sequense):

	pass_bits = random.randint(0, 10)
	for j in range(i, min(len(alice_quantum_sequense), i+pass_bits)):
		if alice_quantum_sequense[j]=='-' or alice_quantum_sequense[j]=='|':
			bob_measure_types += 'x'
		if alice_quantum_sequense[j]=='\\' or alice_quantum_sequense[j]=='/':
			bob_measure_types += '+'

		i += 1;

	if key_idx == len(key_binary) or key_idx==len(alice_quantum_sequense):
		continue;

	if key_binary[key_idx]=='0':
		if alice_quantum_sequense[i]=='-' or alice_quantum_sequense[i]=='/':
			bob_measure_types += '+' if alice_quantum_sequense[i]=='-' else 'x';
			i += 1;
			key_idx += 1;
	if key_binary[key_idx]=='1':
		if alice_quantum_sequense[i]=='|' or alice_quantum_sequense[i]=='\\':
			bob_measure_types += '+' if alice_quantum_sequense[i]=='|' else 'x';
			i += 1;
			key_idx += 1;	

print "[A <--- B] (quantum channel)\n"
print bob_measure_types if key_idx==len(key_binary) else "Error!"
print "\n===============================================================";



### Alice ###

alice_measure_types_correction = "";
for i in range(0, len(alice_quantum_sequense)):
	if alice_quantum_sequense[i]=="|" or alice_quantum_sequense[i]=="-" :
		alice_measure_types_correction += "v" if bob_measure_types[i]=="+" else "_"
	if alice_quantum_sequense[i]=="\\" or alice_quantum_sequense[i]=="/":
		alice_measure_types_correction += "v" if bob_measure_types[i]=="x" else "_"

print "[A ---> B] (quantum channel)\n"
print alice_measure_types_correction
print "\n===============================================================";